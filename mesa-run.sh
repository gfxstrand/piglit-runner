#! /bin/bash
set -e

MESA_RUN_PWD_BACKUP=`pwd`

source `dirname $0`/mesa-build-helper.sh

MESA_REF=$1
shift

build_mesa

cd "${MESA_RUN_PWD_BACKUP}"

exec $*
